# MusicRaft
## Extensions

###plugins
__pyraft plugin (non-esssential)__

A separate plugin `pyraft` supports editing of python3 source files. When a python script writes output with an HTML header,
pyraft dsiplays the HTML in a special 'Html' tab within  the display notebook.
This functionality is present in the 'git' project but absent in the python package,
in order to keep the number of dependencies on other packages to a minimum.

__freqraft plugin (not yet - if ever - available)__

I originally had big ideas for this 'instrument tuning' plugin.
For example, not only the fundamental note but also the audible 
overtones are visible as an an FFT spectrum, so one can see the effect of
veriations in playing technique. In practice, it is more convenient to
use one of the many (free android) mobile apps for general instrument
tuning purpose, especially in ensemble context so I may drop
devlopment on this finally soon.

__other ideas__

Another big idea of mine is to support synchronised display of
multiple parts on diffrent screens as an aid to ensmeble playing.
I'm not yet sure whether musicraft is teh rght vehicle for such
an exercise. 


