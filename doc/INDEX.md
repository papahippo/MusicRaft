# MusicRaft
## Documentation Index

You can find the bare essentials on how to get musicraft up and running
in file [`README.md`](../README.md) in our parent directory.

This directory contains a number of markdown formatted files (*.md),
each addressing a particular area of functionality of musicraft.
I've only just made a start; this is all I've got so far:

* [Architecture](architecture.md)
* [MIDI support](midi.md)

