import sys
if sys.stdout.isatty():
    print("You're running in a real terminal")
else:
    print("You're being piped or redirected")
